Name:           ukui-menu
Version:        4.0.0.9
Release:        3
Summary:        Advanced ukui menu
License:        GPL-3.0+ and Expat
URL:            http://www.ukui.org
Source0:        %{name}-%{version}.tar.gz

Patch01:        ukui-menu-4.0.0.9-modify-install-dir.patch

BuildRequires: cmake
BuildRequires: pkgconf
BuildRequires: qt5-qtbase-devel
BuildRequires: qtchooser
BuildRequires: qt5-qtdeclarative-devel
BuildRequires: qt5-qttools-devel
BuildRequires: qt5-qtx11extras-devel
BuildRequires: kf5-kwindowsystem-devel
BuildRequires: libqtxdg-devel
BuildRequires: libX11-devel
BuildRequires: libxcb-devel
BuildRequires: glib2-devel 
BuildRequires: libukui-search-devel >= 4.0.0.0
BuildRequires: gsettings-qt-devel
BuildRequires: libkysdk-waylandhelper-devel

Requires: ukui-menu-recent-file

%description
 UKUI menu provides start menu development library and advanced
 graphical user interface.
 .
 The package contains executable file.

%package -n libukui-menu-interface1
Summary:	Libraries for ukui-menu-interface.
%description -n libukui-menu-interface1
Libraries for ukui-menu-interface.

%package -n libukui-menu-interface-devel
Summary:	Libraries for ukui-menu-interface(development files).
Requires:   libukui-menu-interface1 = %{version}
%description -n libukui-menu-interface-devel
Libraries for ukui-menu-interface(development files).

%prep
%autosetup -n %{name}-%{version} -p1

%build
%cmake
%{cmake_build}

%install
%{cmake_install}

%files
%license debian/copyright
%doc debian/changelog
%{_bindir}/ukui-menu
%{_sysconfdir}/xdg/autostart/ukui-menu.desktop
%{_datadir}/ukui-menu/translations/*.qm
%{_datadir}/ukui-menu/ukui-menu-global-config.conf
%{_datadir}/dbus-1/services/org.ukui.menu.service
%{_datadir}/glib-2.0/schemas/org.ukui.menu.settings.gschema.xml

%files -n libukui-menu-interface1
%{_libdir}/libukui-menu-interface.so.*
%{_libdir}/qt5/qml/org/ukui/menu/extension/*

%files -n libukui-menu-interface-devel
%{_libdir}/pkgconfig/*.pc
%{_datadir}/cmake/ukui-menu-interface/*.cmake
%{_libdir}/libukui-menu-interface.so
%{_prefix}/include/ukui-menu-interface/*.h

%changelog
* Fri Feb 21 2025 peijiankang <peijiankang@kylinos.cn> - 4.0.0.9-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: add ukui-menu-4.0.0.9-modify-install-dir.patch

* Tue Nov 26 2024 Funda Wang <fundawang@yeah.net> - 4.0.0.9-2
- adopt to new cmake macro

* Sun Apr 07 2024 douyan <douyan@kylinos.cn> - 4.0.0.9-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: update to upstream version 4.0.0.9-ok0.1build1

* Wed Jun 14 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: disable Suspend and Sleep of ukui-menu

* Wed May 24 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-11
- add patch from upstream

* Fri May 12 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-10
- add 0006-changelog.patch 0007-changelog.patch 0008-changelog.patch

* Tue Feb 21 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-9
- add 0005-8-wayland.patch

* Mon Feb 20 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-8
- add 0004-7-close-cd-128489.patch

* Fri Feb 17 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-7
- add execapp

* Thu Feb 16 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-6
- update platformName

* Wed Feb 15 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-5
- add 0003-add-ukui-log4qt.patch

* Fri Feb 10 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-4
- fix coredump of ukui-menu

* Wed Feb 1 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-3
- fix uninstall failed issue

* Tue Jan 17 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.1-2
- add build debuginfo and debugsource

* Thu Dec 01 2022 tanyulong <tanyulong@kylinos.cn> - 3.1.1-1
- update upstream version 3.1.1

* Mon Aug 8 2022 huayadong <huayadong@kylinos.cn> - 3.0.2-11
- Fixed application icon dragging problem

* Mon Aug 8 2022 huayadong <huayadong@kylinos.cn> - 3.0.2-10
- Fixed right click exception popover problem

* Mon Aug 8 2022 huayadong <huayadong@kylinos.cn> - 3.0.2-9
- add optimize the interaction strategy with the taskbar

* Wed Jun 29 2022 huayadong <huayadong@kylinos.cn> - 3.0.2-8
- Fixed size problem with multiple screens

* Fri May 20 2022 tanyulong <tanyulong@kylinos.cn> - 3.0.2-7
- Improve the project according to the requirements of compliance improvement

* Sat Apr 02 2022 tanyulong <tanyulong@kylinos.cn> - 3.0.2-6
- modify yaml file error

* Mon Mar 28 2022 huayadong <huayadong@kylinos.cn> - 3.0.2-5
- Fix crashes when installing or uninstalling software

* Fri Dec 10 2021 huayadong <huayadong@kylinos.cn> - 3.0.2-4
- add patch:0262-Adapt-the-light-color-theme.patch

* Tue Dec 07 2021 huayadong <huayadong@kylinos.cn> - 3.0.2-3
- add patch: 0001-Optimize-Chinese-search.patch

* Mon Oct 26 2020 douyan <douyan@kylinos.cn> - 3.0.2-2
- fix uninstalled failed issue

* Mon Oct 26 2020 douyan <douyan@kylinos.cn> - 3.0.2-1
- update to upstream version 3.0.1-1+1026

* Wen Sep 23 2020 douyan <douyan@kylinos.cn> - 2.0.6-2
- fix uninstall issue

* Mon Jul 20 2020 douyan <douyan@kylinos.cn> - 2.0.6-1
- Init package for openEuler
